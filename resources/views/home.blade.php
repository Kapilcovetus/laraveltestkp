@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <a href="{{ URL::to('products/create') }}">
                    <button type="button" class="btn btn-warning">Add</button>
                  </a>

        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Product Title</th>
              <th scope="col">Product Description</th>
              <th scope="col">Created At</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($products as $product)
            <tr>
              <th scope="row">{{$product->id}}</th>
              <td><a href="/products/{{$product->id}}">{{$product->title}}</a></td>
              <td>{{$product->description}}</td>
              <td>{{$product->created_at->toFormattedDateString()}}</td>
              <td>
              <div class="btn-group" role="group" aria-label="Basic example">
                  <a href="{{ URL::to('products/edit/' . $product->id) }}">
                    <button type="button" class="btn btn-warning">Edit</button>
                  </a>&nbsp;
                  <form action="{{url('products/destroy', [$product->id])}}" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" class="btn btn-danger" value="Delete"/>
                  </form>
              </div>
            </td>
            </tr>
            @endforeach
          </tbody>
        </table>

                

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
