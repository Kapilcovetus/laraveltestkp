@extends('layouts.layout')

@section('content')
    <div class="album text-muted">
      <div class="container">
    <h3 class="h3">shopping Demo-1 </h3>
    <div class="row">
        @foreach($products as $product)

        <div class="col-md-3 col-sm-6">
            <div class="product-grid">
                <div class="product-image">
                    <a href="#">
                        <img style="height:100px;widh:100px;" class="pic-1" src="https://rukminim1.flixcart.com/image/800/960/jsyyufk0/shoe/y/f/f/nitro-8-fitze-black-original-imafefhzyyctdbaj.jpeg?q=50">
                        <div class="fakeimg" style="height:200px;">Image</div>
                    </a>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="#">{{$product->title}}</a></h3>
                    <!-- <div class="price">$16.00
                        <span>$20.00</span>
                    </div> -->
                    <div class="description">
                        {{$product->description}}
                    </div>

                    <!-- <a class="add-to-cart" href="">+ Add To Cart</a> -->
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
    </div>
@endsection