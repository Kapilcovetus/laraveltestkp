@extends('layouts.layout')

    <h1>Edit Product</h1>
    <hr>
     <form action="{{url('products/update', [$product->id])}}" method="POST">
     <input type="hidden" name="_method" value="PUT">
     {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Product Title</label>
        <input type="text" value="{{$product->title}}" class="form-control" id="productTitle"  name="title" >
      </div>
      <div class="form-group">
        <label for="description">Product Description</label>
        <input type="text" value="{{$product->description}}" class="form-control" id="productDescription" name="description" >
      </div>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>