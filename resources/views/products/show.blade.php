@extends('layouts.layout')

            <h1>Showing Product {{ $product->title }}</h1>

    <div class="jumbotron text-center">
        <p>
            <strong>Product Title:</strong> {{ $product->title }}<br>
            <strong>Description:</strong> {{ $product->description }}
        </p>
    </div> 	