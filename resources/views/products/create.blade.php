@extends('layouts.layout')

    <h1>Add New Product</h1>
    <hr>

      {!! Form::open(array('url' =>'products/store', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}    
     {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Product Title</label>
        <input type="text" class="form-control" id="productTitle"  name="title">
      </div>
      <div class="form-group">
        <label for="description">Product Description</label>
        <input type="text" class="form-control" id="productDescription" name="description">
      </div>
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <button type="submit" class="btn btn-primary">Submit</button>
    {!! Form::close() !!}