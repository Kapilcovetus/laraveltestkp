<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/facadeex', function() {
   return TestFacades::testingFacades();
});

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/', 'HomeProductController@index');

Auth::routes();



Route::group(['middleware' => 'auth'], function () {
    // Authentication Routes...
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('products/create', 'ProductController@create');
    Route::post('products/store', 'ProductController@store');
    Route::get('products/show/{id}', 'ProductController@show');
    Route::get('products/edit/{id}', 'ProductController@edit');
    Route::post('products/update', 'ProductController@update');
    Route::get('products/destroy/{id}', 'ProductController@destroy');

});

